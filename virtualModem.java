import ithakimodem.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.awt.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileOutputStream;


public class virtualModem {
    public static void main(String[] args) throws IOException {

        String echoRequestCode = "E3842\r";
        String imageRequestCode = "M2326\r";
        String errorImageRequestCode ="G6262\r";
        String GPSRequestCode = "P0888" + "R=1000050" + "\r";
        String test = "TEST";

        byte[] echoRequestCodeToByte = (new virtualModem()).ConvertToByte(echoRequestCode);
        byte[] imageRequestCodeToByte = (new virtualModem()).ConvertToByte(imageRequestCode);
        byte[] errorImageRequestCodeToByte = (new virtualModem()).ConvertToByte(errorImageRequestCode);
        byte[] GPSRequestCodeToByte = (new virtualModem()).ConvertToByte(GPSRequestCode);


        //(new virtualModem()).demo();
        (new virtualModem()).ReceiveEchoPackets(echoRequestCodeToByte);
        //(new virtualModem()).imageRequest(imageRequestCodeToByte , Boolean.FALSE) ;
        //(new virtualModem()).imageRequest(errorImageRequestCodeToByte , Boolean.TRUE) ;
        //(new virtualModem()).GpsTracking(GPSRequestCodeToByte);

    }

    public void demo() {

        int k;
        Modem modem;
        modem = new Modem();
        modem.setSpeed(1000);
        modem.setTimeout(2000);

        modem.open("ithaki");

        for (; ;) {
            try {
                k = modem.read();
                if (k == -1) break;
                System.out.print((char) k);
            } catch (Exception x) {
                break;
            }
        }

        modem.close();
    }

    public byte[] ConvertToByte(String code){

        byte[] CodeToBytes = code.getBytes(StandardCharsets.UTF_8);
        return CodeToBytes;

    }

    public void ReceiveEchoPackets(byte[] accessCode){

        int echoPacket;

        Modem modem;
        modem =  new Modem();
        modem.setSpeed(1000);


        modem.open("ithaki");

        modem.write(accessCode);

        for (; ;) {
            try {

                echoPacket = modem.read();
                if (echoPacket == -1) break;
                System.out.print((char) echoPacket);

            } catch (Exception x) {
                break;
            }
        }

        modem.close();

    }

    public String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    public String encodeHexString(byte[] byteArray) {
        StringBuffer hexStringBuffer = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            hexStringBuffer.append(byteToHex(byteArray[i]));
        }
        return hexStringBuffer.toString();
    }

    public byte hexToByte(String hexString) {
        int firstDigit = toDigit(hexString.charAt(0));
        int secondDigit = toDigit(hexString.charAt(1));
        return (byte) ((firstDigit << 4) + secondDigit);
    }

    private int toDigit(char hexChar) {
        int digit = Character.digit(hexChar, 16);
        if(digit == -1) {
            throw new IllegalArgumentException(
                    "Invalid Hexadecimal Character: "+ hexChar);
        }
        return digit;
    }

    public byte[] decodeHexString(String hexString) {
        if (hexString.length() % 2 == 1) {
            throw new IllegalArgumentException(
                    "Invalid hexadecimal String supplied.");
        }

        byte[] bytes = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length(); i += 2) {
            bytes[i / 2] = hexToByte(hexString.substring(i, i + 2));
        }
        return bytes;
    }


    public void imageRequest(byte[] accessCode, boolean Errors)   throws IOException{

        int modemSents;
        int counter = 0;

        ArrayList<Byte> sourceCode = new ArrayList<Byte>();
        byte[] sourcePacket = new byte[200000];
        Path saveImage = Paths.get("C:\\Users\\Skiadas\\Desktop\\ergasia\\ExpectImage\\Image.png");
        Path saveErrorImage = Paths.get("C:\\Users\\Skiadas\\Desktop\\ergasia\\ExpectImage\\ErrorImage.png");


        Modem modem;
        modem = new Modem();
        modem.setSpeed(80000);
        modem.setTimeout(2000);

        modem.open("ithaki");

        for(;;){
            try{
                modemSents = modem.read();
                if(modemSents == -1) break;
                System.out.print((char) modemSents);
            }catch (Exception x) {
                break;
            }
        }

        modem.write(accessCode);

        for(;;){
            try{
                modemSents = modem.read();
                if(modemSents == -1) break;
                System.out.print((byte) modemSents);
                sourcePacket[counter] = (byte)modemSents;
                counter++;
            }catch (Exception x) {
                break;
            }
        }

        String hexString = new String();
        hexString = encodeHexString(sourcePacket);

        System.out.println(hexString);

        hexString = StringUtils.substringBetween(hexString , "ffd8", "ffd9");
        hexString = "ffd8" + hexString + "ffd9";
        //System.out.println(hexString);

        byte [] imagePacket = decodeHexString(hexString);

        InputStream is = new ByteArrayInputStream(imagePacket);
        BufferedImage image = ImageIO.read(is);

        if(Boolean.compare(Errors , Boolean.FALSE) == 0){
        ImageIO.write(image, "png" , saveImage.toFile());
        }
        else ImageIO.write(image, "png" , saveErrorImage.toFile());

        modem.close();


    }

    public void GpsTracking(byte[] accessCode) throws IOException{

        int modemSents = 0;

        Modem modem;
        modem = new Modem();
        modem.setSpeed(80000);

        modem.open("ithaki");


        modem.write(accessCode);

        for(;;){
            try{
                modemSents = modem.read();
                if(modemSents == -1) break;
                //System.out.print((char)modemSents);
            }catch(Exception x){
                break;
            }
        }

        String newAccessCode = "P0888" + "T=225735403737" + "T=225758403737" + "T=225734403737" + "T=225733403724"+"\r";
        byte [] newAccessCodeToByte = ConvertToByte(newAccessCode);
        modem.write(newAccessCodeToByte);
        byte [] sourceCode = new byte[200000];
        int counter = 0;

        for(;;){
            try{
                modemSents = modem.read();
                if(modemSents == -1) break;
                //System.out.print((char)modemSents);
                sourceCode[counter] = (byte)modemSents;
                counter++;
            }catch(Exception x){
                break;
            }
        }

        String hexString = new String();
        hexString = encodeHexString(sourceCode);

        hexString = StringUtils.substringBetween(hexString , "ffd8", "ffd9");
        hexString = "ffd8" + hexString + "ffd9";
        System.out.println(hexString);

        Path saveGPSImage = Paths.get("C:\\Users\\Skiadas\\Desktop\\ergasia\\GPSImage\\GPSImage.png");

        byte[] imagePacket = decodeHexString(hexString);
        InputStream is = new ByteArrayInputStream(imagePacket);
        BufferedImage image = ImageIO.read(is);

        ImageIO.write(image, "png" , saveGPSImage.toFile());



        modem.close();

    }

}